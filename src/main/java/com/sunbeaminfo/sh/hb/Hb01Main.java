package com.sunbeaminfo.sh.hb;

import org.hibernate.Session;

public class Hb01Main {
	public static void main(String[] args) {
		/*
		// without dao
		try(Session session = HbUtil.openSession()) {
			Book b = session.get(Book.class, 11);
			System.out.println("Found : " + b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/

		/*
		try(BookDao dao = new BookDao()) {
			dao.open();
			Book b = dao.getBook(61);
			System.out.println("Found : " + b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		
		/*
		try(BookDao dao = new BookDao()) {
			dao.open();
			Book b = new Book(61, "Abc", "Pqr", "Xyz", 123.45);
			dao.addBook(b);
			System.out.println("Added : " + b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		/*
		try(BookDao dao = new BookDao()) {
			dao.open();
			Book b = dao.getBook(61);
			b.setCost(764.23);
			dao.updateBook(b);
			System.out.println("Updated : " + b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		/*
		try(BookDao dao = new BookDao()) {
			dao.open();
			dao.deleteBook(61);
			System.out.println("Deleted.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		
		// load() demo
		Book b3 = null;
		try(Session session = HbUtil.openSession()) {
			Book b1 = session.load(Book.class, 11); // return proxy obj
			System.out.println("Returned : " + b1.getClass().getName());
			System.out.println("Book Id : " + b1.getId()); // print id in proxy id
			System.out.println("Book Name : " + b1.getTitle()); // exec SELECT & get data
			System.out.println("Book Author : " + b1.getAuthor());
			
			b3 = session.load(Book.class, 22); // return proxy
			
			Book b2 = session.load(Book.class, 71); // return proxy
			System.out.println("Returned : " + b2); // exec SELECT & throw exception ObjectNotFoundException
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Returned : " + b3); // try to exec SELECT, but session closed
			// throw exception LazyInitializationException

		HbUtil.shutdown();
	}
}


