package com.sunbeaminfo.sh.hb;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class BookDao implements AutoCloseable {
	private Session session;
	public void open() {
		session = HbUtil.openSession();
	}
	@Override
	public void close() {
		if(session != null)
			session.close();
	}
	public Book getBook(int id) {
		Book b = session.get(Book.class, id);
		return b;
	}
	public void addBook(Book b) {
		Transaction tx = session.beginTransaction();
		try {
			session.persist(b);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
		}
	}
	
	public void updateBook(Book b) {
		Transaction tx = session.beginTransaction();
		try {
			//session.update(b);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
		}
	}
	
	public void deleteBook(int id) {
		Transaction tx = session.beginTransaction();
		try {
			Book b = session.get(Book.class, 61);
			if(b != null)
				session.remove(b);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
		}
	}
}
